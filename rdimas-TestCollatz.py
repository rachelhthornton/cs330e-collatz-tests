#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, Cache

# -----------
# Cache
# -----------
def test_cycles(n, s=''):
  
    # Used to test the catalog function where no end is given.
    test_cycle_one = [9, 28, 14, 7, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
    test_cycle_two = [20, 10, 5, 16, 8, 4, 2, 1]
    test_cycle_three = [13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
    test_cycle_four = [29, 88, 44, 22, 11, 34, 17, 52, 26, 13, 40, 20, 10, 5, 16, 8, 4, 2, 1]
    
    # Used to test catalog with a given end.
    test_cycle_five = [[72,36,18], [20]]
    test_cycle_six = [[26,13,40], [8]]
    test_cycle_seven = [[104, 52,26], [10]]
    
    
    # Reference dictionaries for the catalog and get length unit tests.
    test_dict_one = {9: 20, 28: 19, 14: 18,7: 17, 22: 16, 11: 15,
                  34: 14, 17: 13, 52: 12, 26: 11, 13: 10, 40: 9,
                  20: 8, 10: 7, 5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1}
    
    test_dict_two = {20: 8, 10: 7, 5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1}


    test_dict_three = {13: 10, 40: 9, 20: 8, 10: 7, 5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1}
    
    test_dict_four  = {29: 19, 88: 18, 44: 17, 22: 16, 11: 15, 34: 14,
                  17: 13, 52: 12, 26: 11, 13: 10, 40: 9, 20: 8, 10: 7,
                  5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1}
    
    test_dict_five = {72: 23, 36: 22, 18:21, 9: 20, 28: 19, 14: 18,7: 17, 22: 16, 11: 15,
                  34: 14, 17: 13, 52: 12, 26: 11, 13: 10, 40: 9,
                  20: 8, 10: 7, 5: 6, 16: 5, 8: 4, 4: 3, 2: 2, 1: 1}
    
    test_dict_six = {26: 11, 13: 10, 40:9, 
                     20: 8, 10: 7, 5: 6, 16: 5, 
                     8: 4, 4: 3, 2: 2, 1: 1}
    
    test_dict_seven = {104: 13, 52: 12, 26: 11, 13: 10, 
                       40: 9, 20: 8, 10: 7, 5: 6, 16: 5, 
                       8: 4, 4: 3, 2: 2, 1: 1}

    test_cycles = [test_cycle_one, test_cycle_two, 
                   test_cycle_three, test_cycle_four, 
                   test_cycle_five, test_cycle_six, 
                   test_cycle_seven]
    
    test_dict = [test_dict_one , test_dict_two , 
                 test_dict_three , test_dict_four, 
                 test_dict_five, test_dict_six, 
                 test_dict_seven]

    if s == 'dict':
        return test_dict[n]
    else:
        return test_cycles[n]
    
# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)
        
    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 200)
        
    def test_read_3(self):
        s = "201 210\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  201)
        self.assertEqual(j, 210)

    def test_read_4(self):
        s = "900 1000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  900)
        self.assertEqual(j, 1000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")
        
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 201, 210, 89)
        self.assertEqual(w.getvalue(), "201 210 89\n")
        
    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 900, 100, 174)
        self.assertEqual(w.getvalue(), "900 100 174\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 1\n2 4\n326 432\n1000 1003\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n2 4 8\n326 432 144\n1000 1003 143\n")
        
    def test_solve_3(self):
        r = StringIO("1 600\n10000 10001\n25 26\n7 40\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 600 144\n10000 10001 180\n25 26 24\n7 40 112\n")
    def test_solve_4(self):
        r = StringIO("89 34\n90 91\n99 102\n103 345\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "89 34 116\n90 91 93\n99 102 26\n103 345 144\n")


    
    # -------
    # catalog
    # -------
    def test_catalog_1(self):
        cache = Cache()
        cycle = test_cycles(0)
        re_dict = test_cycles(0, 'dict')
        
        cache.catalog(cycle)
        shared_items = {k: cache.dict[k] for k in cache.dict if
                         k in re_dict and cache.dict[k] == re_dict[k]}
        self.assertEqual(len(shared_items), 20)
        
    def test_catalog_2(self):
        cache = Cache()
        cycle = test_cycles(1)
        re_dict = test_cycles(1, 'dict')
        
        cache.catalog(cycle)
        shared_items = {k: cache.dict[k] for k in cache.dict if
                         k in re_dict and cache.dict[k] == re_dict[k]}
        self.assertEqual(len(shared_items), 8)
        
    def test_catalog_3(self):
        cache = Cache()
        cycle = test_cycles(2)
        re_dict = test_cycles(2, 'dict')
        
        cache.catalog(cycle)
        shared_items = {k: cache.dict[k] for k in cache.dict if
                         k in re_dict and cache.dict[k] == re_dict[k]}
        self.assertEqual(len(shared_items), 10)
        
    def test_catalog_4(self):
        cache = Cache()
        cycle = test_cycles(3)
        re_dict = test_cycles(3, 'dict')
        
        cache.catalog(cycle)
        shared_items = {k: cache.dict[k] for k in cache.dict if
                         k in re_dict and cache.dict[k] == re_dict[k]}
        self.assertEqual(len(shared_items), 19)
        
    def test_catalog_5(self):
        cache = Cache()
        cycle = test_cycles(0)
        cycle_two = test_cycles(4)
        re_dict = test_cycles(4, 'dict')
        
        cache.catalog(cycle)
        cache.catalog(cycle_two[0], cycle_two[1][0])
        shared_items = {k: cache.dict[k] for k in cache.dict if
                         k in re_dict and cache.dict[k] == re_dict[k]}
        self.assertEqual(len(shared_items), 23)
        
    def test_catalog_6(self):
        cache = Cache()
        cycle = test_cycles(1)
        cycle_two = test_cycles(5)
        re_dict = test_cycles(5, 'dict')
        
        cache.catalog(cycle)
        cache.catalog(cycle_two[0], cycle_two[1][0])
        shared_items = {k: cache.dict[k] for k in cache.dict if
                         k in re_dict and cache.dict[k] == re_dict[k]}
        self.assertEqual(len(shared_items), 11)
        
    def test_catalog_7(self):
        cache = Cache()
        cycle = test_cycles(2)
        cycle_two = test_cycles(6)
        re_dict = test_cycles(6, 'dict')
        
        cache.catalog(cycle)
        cache.catalog(cycle_two[0], cycle_two[1][0])
        shared_items = {k: cache.dict[k] for k in cache.dict if
                         k in re_dict and cache.dict[k] == re_dict[k]}
        self.assertEqual(len(shared_items), 13)
        
    # ----------
    # get_length
    # ----------

    def test_get_length_1(self):
        cache = Cache()
        cache.dict = test_cycles(0, 'dict')
        v = cache.get_length(14)
        self.assertEqual(v, 18)

    def test_get_length_2(self):
        cache = Cache()
        cache.catalog(test_cycles(1))
        v = cache.get_length(5)
        self.assertEqual(v, 6)

    def test_get_length_3(self):
        cache = Cache()
        cache.catalog(test_cycles(2))
        v = cache.get_length(13)
        self.assertEqual(v, 10)
        
    def test_get_length_4(self):
        cache = Cache()
        cache.catalog(test_cycles(3))
        v = cache.get_length(52)
        self.assertEqual(v, 12)
    
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
