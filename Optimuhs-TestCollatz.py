#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "100 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "9999 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 9999)
        self.assertEqual(j, 100)
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(9, 1000)
        self.assertEqual(v, 179)
    
    def test_eval_6(self):
        v = collatz_eval(1, 20)
        self.assertEqual(v, 21)
    
    def test_eval_7(self):
        v = collatz_eval(1, 20000)
        self.assertEqual(v, 279)

    def test_eval_8(self):
        v = collatz_eval(200000, 999999)
        self.assertEqual(v, 525)
    
    def test_eval_9(self):
        v = collatz_eval(20, 45000)
        self.assertEqual(v, 324)
    
    def test_eval_10(self):
        v = collatz_eval(7777, 8888)
        self.assertEqual(v, 252)

    def test_eval_11(self):
        v = collatz_eval(858, 404040)
        self.assertEqual(v, 443)
    
    def test_eval_12(self):
        v = collatz_eval(22, 2222)
        self.assertEqual(v, 182)
    
    def test_eval_13(self):
        v = collatz_eval(45, 9999)
        self.assertEqual(v, 262)

    def test_eval_14(self):
        v = collatz_eval(99998, 4445)
        self.assertEqual(v, 351)
    
    def test_eval_15(self):
        v = collatz_eval(8830, 4999)
        self.assertEqual(v, 262)

    def test_eval_16(self):
        v = collatz_eval(278, 888)
        self.assertEqual(v, 179)

    def test_eval_17(self):
        v = collatz_eval(999999, 800000)
        self.assertEqual(v, 525)
    
    def test_eval_18(self):
        v = collatz_eval(75, 7000)
        self.assertEqual(v, 262)
    
    def test_eval_19(self):
        v = collatz_eval(690, 960)
        self.assertEqual(v, 179)

    def test_eval_20(self):
        v = collatz_eval(149, 9410)
        self.assertEqual(v, 262)
    
    def test_eval_21(self):
        v = collatz_eval(1109, 899)
        self.assertEqual(v, 174)

    def test_eval_22(self):
        v = collatz_eval(809287, 960380)
        self.assertEqual(v, 525)

    def test_eval_23(self):
        v = collatz_eval(424831, 22632)
        self.assertEqual(v, 449)

    def test_eval_24(self):
        v = collatz_eval(608723, 498796)
        self.assertEqual(v, 470)

    def test_eval_25(self):
        v = collatz_eval(508638, 701240)
        self.assertEqual(v, 509)

    def test_eval_26(self):
        v = collatz_eval(288114, 36373)
        self.assertEqual(v, 443)
    
    def test_eval_27(self):
        v = collatz_eval(638214, 252678)
        self.assertEqual(v, 509)
    
    def test_eval_28(self):
        v = collatz_eval(603518, 896826)
        self.assertEqual(v, 525)

    def test_eval_29(self):
        v = collatz_eval(532751, 924306)
        self.assertEqual(v, 525)
    
    def test_eval_30(self):
        v = collatz_eval(110632, 500650)
        self.assertEqual(v, 449)

    def test_eval_31(self):
        v = collatz_eval(54448, 34367)
        self.assertEqual(v, 340)

    def test_eval_32(self):
        v = collatz_eval(327155, 48513)
        self.assertEqual(v, 443)
    
    def test_eval_33(self):
        v = collatz_eval(80915, 866700)
        self.assertEqual(v, 525)
    
    def test_eval_34(self):
        v = collatz_eval(816531, 660148)
        self.assertEqual(v, 504)

    def test_eval_35(self):
        v = collatz_eval(465140, 760564)
        self.assertEqual(v, 509)
    
    def test_eval_36(self):
        v = collatz_eval(522819, 644589)
        self.assertEqual(v, 509)

    def test_eval_37(self):
        v = collatz_eval(358207, 934598)
        self.assertEqual(v, 525)

    def test_eval_38(self):
        v = collatz_eval(958856, 839719)
        self.assertEqual(v, 507)
    
    def test_eval_39(self):
        v = collatz_eval(92858, 80611)
        self.assertEqual(v, 333)
    
    def test_eval_40(self):
        v = collatz_eval(108977, 713197)
        self.assertEqual(v, 509)

    def test_eval_41(self):
        v = collatz_eval(162105, 587725)
        self.assertEqual(v, 470)
    
    def test_eval_42(self):
        v = collatz_eval(921408, 633007)
        self.assertEqual(v, 525)

    def test_eval_43(self):
        v = collatz_eval(263882, 541640)
        self.assertEqual(v, 470)

    def test_eval_44(self):
        v = collatz_eval(233218, 190904)
        self.assertEqual(v, 443)

    def test_eval_45(self):
        v = collatz_eval(540932, 536275)
        self.assertEqual(v, 408)

    def test_eval_46(self):
        v = collatz_eval(119594, 484337)
        self.assertEqual(v, 449)

    def test_eval_47(self):
        v = collatz_eval(562455, 563554)
        self.assertEqual(v, 359)
    
    def test_eval_48(self):
        v = collatz_eval(69173, 168141)
        self.assertEqual(v, 383)
    
    def test_eval_49(self):
        v = collatz_eval(510137, 389689)
        self.assertEqual(v, 449)

    def test_eval_50(self):
        v = collatz_eval(996673, 14037)
        self.assertEqual(v, 525)
    
    def test_eval_51(self):
        v = collatz_eval(308327, 334540)
        self.assertEqual(v, 384)

    def test_eval_52(self):
        v = collatz_eval(660057, 736351)
        self.assertEqual(v, 504)

    def test_eval_53(self):
        v = collatz_eval(117967, 359791)
        self.assertEqual(v, 443)
    
    def test_eval_54(self):
        v = collatz_eval(467329, 536544)
        self.assertEqual(v, 470)
    
    def test_eval_55(self):
        v = collatz_eval(269870, 118772)
        self.assertEqual(v, 443)

    def test_eval_56(self):
        v = collatz_eval(273063, 409426)
        self.assertEqual(v, 441)
    
    def test_eval_57(self):
        v = collatz_eval(214896, 326982)
        self.assertEqual(v, 443)

    def test_eval_58(self):
        v = collatz_eval(848703, 529306)
        self.assertEqual(v, 525)

    def test_eval_59(self):
        v = collatz_eval(464602, 861611)
        self.assertEqual(v, 525)
    
    def test_eval_60(self):
        v = collatz_eval(825762, 926790)
        self.assertEqual(v, 525)
    
    def test_eval_61(self):
        v = collatz_eval(304784, 577805)
        self.assertEqual(v, 470)

    def test_eval_62(self):
        v = collatz_eval(135344, 800089)
        self.assertEqual(v, 509)
    
    def test_eval_63(self):
        v = collatz_eval(829803, 875447)
        self.assertEqual(v, 525)

    def test_eval_64(self):
        v = collatz_eval(592689, 380657)
        self.assertEqual(v, 470)

    def test_eval_65(self):
        v = collatz_eval(313096, 908569)
        self.assertEqual(v, 525)

    def test_eval_66(self):
        v = collatz_eval(829351, 670033)
        self.assertEqual(v, 504)

    def test_eval_67(self):
        v = collatz_eval(444310, 603549)
        self.assertEqual(v, 470)

    def test_eval_68(self):
        v = collatz_eval(696092, 13101)
        self.assertEqual(v, 509)
    
    def test_eval_69(self):
        v = collatz_eval(920806, 446594)
        self.assertEqual(v, 525)
    
    def test_eval_70(self):
        v = collatz_eval(73736, 20092)
        self.assertEqual(v, 340)

    def test_eval_71(self):
        v = collatz_eval(914208, 806886)
        self.assertEqual(v, 525)
    
    def test_eval_72(self):
        v = collatz_eval(11986, 555162)
        self.assertEqual(v, 470)

    def test_eval_73(self):
        v = collatz_eval(168785, 583554)
        self.assertEqual(v, 470)

    def test_eval_74(self):
        v = collatz_eval(247966, 546338)
        self.assertEqual(v, 470)
    
    def test_eval_75(self):
        v = collatz_eval(148403, 586511)
        self.assertEqual(v, 470)
    
    def test_eval_76(self):
        v = collatz_eval(185039, 660150)
        self.assertEqual(v, 509)

    def test_eval_77(self):
        v = collatz_eval(353147, 402688)
        self.assertEqual(v, 436)
    
    def test_eval_78(self):
        v = collatz_eval(919030, 839848)
        self.assertEqual(v, 476)

    def test_eval_79(self):
        v = collatz_eval(255018, 534692)
        self.assertEqual(v, 470)

    def test_eval_80(self):
        v = collatz_eval(855459, 667039)
        self.assertEqual(v, 525)
    
    def test_eval_81(self):
        v = collatz_eval(963288, 84563)
        self.assertEqual(v, 525)
    
    def test_eval_82(self):
        v = collatz_eval(325934, 710921)
        self.assertEqual(v, 509)

    def test_eval_83(self):
        v = collatz_eval(510704, 943302)
        self.assertEqual(v, 525)
    
    def test_eval_84(self):
        v = collatz_eval(697413, 186325)
        self.assertEqual(v, 509)
    
    def test_eval_85(self):
        v = collatz_eval(907375, 218300)
        self.assertEqual(v, 525)

    def test_eval_86(self):
        v = collatz_eval(294237, 118689)
        self.assertEqual(v, 443)

    def test_eval_87(self):
        v = collatz_eval(417222, 685603)
        self.assertEqual(v, 509)

    def test_eval_88(self):
        v = collatz_eval(22646, 718348)
        self.assertEqual(v, 509)

    def test_eval_89(self):
        v = collatz_eval(354414, 866650)
        self.assertEqual(v, 525)
    
    def test_eval_90(self):
        v = collatz_eval(896255, 833686)
        self.assertEqual(v, 525)
    
    def test_eval_91(self):
        v = collatz_eval(856269, 716584)
        self.assertEqual(v, 525)

    def test_eval_92(self):
        v = collatz_eval(467413, 464996)
        self.assertEqual(v, 369)
    
    def test_eval_93(self):
        v = collatz_eval(893035, 446028)
        self.assertEqual(v, 525)
    
    def test_eval_94(self):
        v = collatz_eval(138493, 104256)
        self.assertEqual(v, 354)

    def test_eval_95(self):
        v = collatz_eval(756722, 919449)
        self.assertEqual(v, 525)
    
    def test_eval_96(self):
        v = collatz_eval(5557, 491859)
        self.assertEqual(v, 449)
    
    def test_eval_97(self):
        v = collatz_eval(498567, 156458 )
        self.assertEqual(v, 449)

    def test_eval_98(self):
        v = collatz_eval(603353, 453112 )
        self.assertEqual(v, 470)
    
    def test_eval_99(self):
        v = collatz_eval(757518, 582743)
        self.assertEqual(v, 509)

    def test_eval_100(self):
        v = collatz_eval(922349, 813061)
        self.assertEqual(v, 525)
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 9999, 1000, 262)
        self.assertEqual(w.getvalue(), "9999 1000 262\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w,998, 998, 50 )
        self.assertEqual(w.getvalue(), "998 998 50\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO('''1 10\n100 200\n201 210\n900 1000\n9 1000\n1 20\n1 20000\n1 200000\n200000 999999\n''')
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), '''1 10 20\n100 200 125\n201 210 89\n900 1000 174\n9 1000 179\n1 20 21\n1 20000 279\n1 200000 383\n200000 999999 525\n''')

    def test_solve_2(self):
        r = StringIO("20 45000\n7777 8888\n858 404040\n22 2222\n45 9999\n99998 4445\n8830 4999\n278 888\n999999 800000\n75 7000\n690 960\n149 9410\n1109 899\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "20 45000 324\n7777 8888 252\n858 404040 443\n22 2222 182\n45 9999 262\n99998 4445 351\n8830 4999 262\n278 888 179\n999999 800000 525\n75 7000 262\n690 960 179\n149 9410 262\n1109 899 174\n")
   
    def test_solve_3(self):
        r = StringIO("1000 1000\n 9000 9000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1000 1000 112\n9000 9000 48\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
